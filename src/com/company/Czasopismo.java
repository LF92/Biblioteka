package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public class Czasopismo extends Pozycja {

    private int numer;

    public Czasopismo(String tytul, int id, String wydawnictwo, int rokWydania, int numer) {
        super(tytul, id, wydawnictwo, rokWydania);
        this.numer = numer;
    }

    public int getNumer() {
        return numer;
    }

    public void setNumer(int numer) {
        this.numer = numer;
    }

    @Override
    public void wypiszInfo() {
        super.wypiszInfo();
        System.out.println("Numer:\t\t\t" + numer);
    }
}

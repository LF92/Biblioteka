package com.company;

public class Main {

    public static void main(String[] args) {

        Autor autorLC = new Autor("Lewis", "Carroll", "Wielka Brytania");
        Autor autorAM = new Autor("Adam", "Mickiewicz", "Polska");
        Osoba autorJS = new Autor("Juliusz", "Słowacki", "Polska");

        Bibliotekarz bibliotekarzRK = new Bibliotekarz("Rafał", "Kos", "18.02.2015", 3800.0);
        Osoba bibliotekarzMN = new Bibliotekarz("Małgorzata", "Nowak","08.10.2013", 4200.0);

        Biblioteka biblioteka1 = new Biblioteka("ul. Reymonta 12");

        biblioteka1.dodajBibliotekarza(bibliotekarzRK);
        biblioteka1.dodajBibliotekarza(((Bibliotekarz)bibliotekarzMN));
        biblioteka1.wypiszBibliotekarzy();

        Ksiazka ksiazkaAWKCz = new Ksiazka("Alicja w Krainie Czarów", 123589, "Buchmann", 2013, 192);
        Ksiazka ksiazkaKoralina = new Ksiazka("Koralina", 125847,"Mag", 2009, 182);
        Pozycja ksiazkaMakbet = new Ksiazka("Makbet", 12569, "GREG", 2002, 1236);
        Czasopismo czasopismoSwiatWiedzy = new Czasopismo("Świat wiedzy", 12584, "Bauer", 2017, 5);

        ksiazkaAWKCz.wypiszInfo();
        ksiazkaMakbet.wypiszInfo();
        czasopismoSwiatWiedzy.wypiszInfo();

        ksiazkaAWKCz.dodajAutora(autorLC);
        ((Ksiazka)ksiazkaMakbet).dodajAutora("William", "Shakespeare","XX");

        System.out.println("##########################");

        Katalog katalog = new Katalog("Dla dzieci");
        katalog.dodajPozycje(ksiazkaAWKCz);
        katalog.dodajPozycje(ksiazkaKoralina);
        katalog.znajdzPozycjePoId(123589);

        System.out.println("##########################");

        katalog.wypiszWszystkiePozycje();

        System.out.println("############+++##############");

        biblioteka1.dodajKatalog(katalog);

        biblioteka1.znajdzPozycjePoTytule("Koralina");

        System.out.println("############+++##############");
        
        biblioteka1.znajdzPozycjePoId(123589);


    }
}

package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public class Osoba {

    private String imie;
    private String nazwisko;

    public Osoba(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void wypiszInfo() {
        System.out.print(String.format("Imię i nazwisko: %s %s ", imie, nazwisko));
    }
}

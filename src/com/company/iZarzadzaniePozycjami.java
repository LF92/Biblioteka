package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public interface iZarzadzaniePozycjami {

    void znajdzPozycjePoTytule(String tytlu);
    void znajdzPozycjePoId(int id);
    void wypiszWszystkiePozycje();
}

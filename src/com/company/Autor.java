package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public class Autor extends Osoba {

    private String narodowosc;

    public Autor(String imie, String nazwisko, String narodowosc) {
        super(imie, nazwisko);
        this.narodowosc = narodowosc;
    }

    public String getNarodowosc() {
        return narodowosc;
    }

    public void setNarodowosc(String narodowosc) {
        this.narodowosc = narodowosc;
    }
}

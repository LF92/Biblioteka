package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public abstract class Pozycja {

    private String tytul;
    private int id;
    private String wydawnictwo;
    private int rokWydania;

    public Pozycja(String tytul, int id, String wydawnictwo, int rokWydania) {
        this.tytul = tytul;
        this.id = id;
        this.wydawnictwo = wydawnictwo;
        this.rokWydania = rokWydania;
    }

    public void wypiszInfo() {
        System.out.println(String.format("\nTytuł:\t\t\t%s\nID:\t\t\t\t%d\nWydawnictwo:\t%s\nRok wydania:\t%s", tytul,
                id, wydawnictwo, rokWydania));
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWydawnictwo() {
        return wydawnictwo;
    }

    public void setWydawnictwo(String wydawnictwo) {
        this.wydawnictwo = wydawnictwo;
    }

    public int getRokWydania() {
        return rokWydania;
    }

    public void setRokWydania(int rokWydania) {
        this.rokWydania = rokWydania;
    }
}

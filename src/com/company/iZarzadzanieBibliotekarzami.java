package com.company;

/**
 * Created by lukasz on 25.04.17.
 */
public interface iZarzadzanieBibliotekarzami {

    void znajdzBibliotekarza(String imie, String nazwisko);
    void znajdzBibliotekarza(String nazwisko);
    void znajdzBibliotekarzaPoDacieZatrudnienia(String dataZatrudnienia);
    void wypiszBibliotekarzy();
}

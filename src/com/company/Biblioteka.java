package com.company;

import java.util.ArrayList;

/**
 * Created by lukasz on 24.04.17.
 */
public class Biblioteka implements iZarzadzaniePozycjami, iZarzadzanieBibliotekarzami {

    private String adres;
    private ArrayList<Bibliotekarz> listaBibliotekarzy = new ArrayList<>();
    private ArrayList<Katalog> listaKatalogow = new ArrayList<>();

    public Biblioteka(String adres) {
        this.adres = adres;
    }

    public void dodajBibliotekarza(Bibliotekarz bibliotekarz) {
        listaBibliotekarzy.add(bibliotekarz);
    }

    @Override
    public void znajdzBibliotekarza(String imie, String nazwisko) {
        for (Bibliotekarz bibliotekarz : listaBibliotekarzy) {
            if (bibliotekarz.getImie().equals(imie) && bibliotekarz.getNazwisko().equals(nazwisko)) {
                bibliotekarz.wypiszInfo();
            }
        }
    }

    @Override
    public void znajdzBibliotekarza(String nazwisko) {
        for (Bibliotekarz bibliotekarz : listaBibliotekarzy) {
            if ( bibliotekarz.getNazwisko().equals(nazwisko)) {
                bibliotekarz.wypiszInfo();
            }
        }
    }

    @Override
    public void znajdzBibliotekarzaPoDacieZatrudnienia(String dataZatrudnienia) {
        for (Bibliotekarz bibliotekarz : listaBibliotekarzy) {
            if ( bibliotekarz.getNazwisko().equals(dataZatrudnienia)) {
                bibliotekarz.wypiszInfo();
            }
        }
    }

    public void wypiszBibliotekarzy() {
        for (Bibliotekarz bibliotekarz : listaBibliotekarzy) {
            bibliotekarz.wypiszInfo();
        }
    }

    public void dodajKatalog(Katalog katalog)
     {
        listaKatalogow.add(katalog);
    }

    public void dodajPozycje(Pozycja pozycja, String dzialTematyczny) {
        for (Katalog katalog : listaKatalogow) {
            if (katalog.getDzialTematyczny().equals(dzialTematyczny)) {
                katalog.dodajPozycje(pozycja);
            }
        }
    }

    @Override
    public void znajdzPozycjePoTytule(String tytul) {
        for (Katalog katalog : listaKatalogow) {
            katalog.znajdzPozycjePoTytule(tytul);
        }
    }

    @Override
    public void znajdzPozycjePoId(int id) {
        for (Katalog katalog : listaKatalogow) {
            katalog.znajdzPozycjePoId(id);
        }
    }

    @Override
    public void wypiszWszystkiePozycje() {
        for (Katalog katalog : listaKatalogow) {
            katalog.wypiszWszystkiePozycje();
        }
    }
}

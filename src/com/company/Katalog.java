package com.company;

import java.util.ArrayList;

/**
 * Created by lukasz on 24.04.17.
 */
public class Katalog implements iZarzadzaniePozycjami {

    private String dzialTematyczny;
    private ArrayList<Pozycja> listaPozycji  = new ArrayList<>();

    public Katalog(String dzialTematyczny) {
        this.dzialTematyczny = dzialTematyczny;
    }

    public String getDzialTematyczny() {
        return dzialTematyczny;
    }

    public void setDzialTematyczny(String dzialTematyczny) {
        this.dzialTematyczny = dzialTematyczny;
    }

    public ArrayList<Pozycja> getListaPozycji() {
        return listaPozycji;
    }

    public void setListaPozycji(ArrayList<Pozycja> listaPozycji) {
        this.listaPozycji = listaPozycji;
    }

    public void dodajPozycje(Pozycja pozycja) {
        listaPozycji.add(pozycja);
    }

    @Override
    public void znajdzPozycjePoTytule(String tytlu) {

        boolean czyZnaleziono = false;

        for (Pozycja pozycja : listaPozycji) {
            if (pozycja.getTytul().equals(tytlu)) {
                czyZnaleziono = true;
                System.out.println("ZNALEZIONO: ");
                pozycja.wypiszInfo();
            }
        }

        if(!czyZnaleziono)
            System.out.println("Nie znaleziono");
    }

    @Override
    public void znajdzPozycjePoId(int id) {
        boolean czyZnaleziono = false;

        for (Pozycja pozycja : listaPozycji) {
            if (pozycja.getId() == id) {
                czyZnaleziono = true;
                System.out.println("ZNALEZIONO: ");
                pozycja.wypiszInfo();
            }
        }

        if(!czyZnaleziono)
            System.out.println("Nie znaleziono");
    }

    @Override
    public void wypiszWszystkiePozycje() {
        if (listaPozycji.isEmpty()) {
            System.out.println("Brak pozycji");
        } else {
            for (Pozycja pozycja : listaPozycji) {
                pozycja.wypiszInfo();
            }
        }
    }
}

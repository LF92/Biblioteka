package com.company;

/**
 * Created by lukasz on 24.04.17.
 */
public class Bibliotekarz extends Osoba {

    private String dataZatrudnienia;
    private double wynagrodzenie;

    public Bibliotekarz(String imie, String nazwisko, String dataZatrudnienia, double wynagrodzenie) {
        super(imie, nazwisko);
        this.dataZatrudnienia = dataZatrudnienia;
        this.wynagrodzenie = wynagrodzenie;
    }

    public String getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public void setDataZatrudnienia(String dataZatrudnienia) {
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getWynagrodzenie() {
        return wynagrodzenie;
    }

    public void setWynagrodzenie(double wynagrodzenie) {
        this.wynagrodzenie = wynagrodzenie;
    }

    @Override
    public void wypiszInfo() {
        super.wypiszInfo();
        System.out.println(String.format(", data zatrudnienia: %s, wynagrodzenie: %s", dataZatrudnienia, wynagrodzenie));
    }
}

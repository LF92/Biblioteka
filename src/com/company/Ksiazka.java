package com.company;

import java.util.ArrayList;

/**
 * Created by lukasz on 24.04.17.
 */
public class Ksiazka extends Pozycja {

    private int liczbaStron;
    private ArrayList<Autor> listaAutorow = new ArrayList<>();

    public Ksiazka(String tytul, int id, String wydawnictwo, int rokWydania, int liczbaStron) {
        super(tytul, id, wydawnictwo, rokWydania);
        this.liczbaStron = liczbaStron;
    }

    public int getLiczbaStron() {
        return liczbaStron;
    }

    public void setLiczbaStron(int liczbaStron) {
        this.liczbaStron = liczbaStron;
    }

    public void dodajAutora(Autor autor) {
        listaAutorow.add(autor);
    }

    public void dodajAutora(String imie, String nazwisko, String narodowosc) {
        listaAutorow.add(new Autor(imie, nazwisko, narodowosc));
    }

    @Override
    public void wypiszInfo() {
        super.wypiszInfo();
        System.out.println("Liczba stron:\t" + liczbaStron);

        if(!listaAutorow.isEmpty()) {
            System.out.println("Autorzy:");
            for (Autor autor : listaAutorow) {
                System.out.println(autor.getImie() + " " + autor.getNazwisko());
            }
        }
    }
}
